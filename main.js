import anime from "./modules/anime.es.js";
import { createIntersectionObserver } from "./modules/observer.js";

const template = document.querySelector(".section");
template.classList.add("hidden");
const wrapper = document.querySelector("main");
let observerOptions = {
  root: null,
  rootMargin: "0px",
  threshold: [],
};
let thresholdSets = [
  [],
  [0.5],
  [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0],
  [0, 0.25, 0.5, 0.75, 1.0],
];
for (let i = 0; i <= 1.0; i += 0.01) {
  thresholdSets[0].push(i);
}
/**
 *
 * @param {HTMLDivElement} section
 * @return {{section: HTMLDivElement, container: HTMLDivElement, texts: HTMLParagraphElement}}
 */
const createComponentState = (section) => {
  const component = {
    section: section,
    container: section.querySelector(".container"),
    texts: section.querySelectorAll(".section__text"),
    observer: null,
  };
  const animation = anime({
    targets: component.texts,
    translateY: 250,
    elasticity: 200,
    opacity: 1,
    easing: "easeInOutSine",
    autoplay: false,
  });
  component.observer = createIntersectionObserver(
    section,
    (entries, observer) => {
      entries.forEach((entry) => {
        let box = entry.target;
        let visiblePct = (Math.floor(entry.intersectionRatio * 100) + 1) * 2;
        box.querySelector(".info").innerHTML = `${visiblePct}%`;
        animation.seek((visiblePct / 100) * animation.duration);
      });
    },
    observerOptions
  );
  return component;
};

for (let i = 0; i < 4; i++) {
  const element = template.cloneNode(true);
  observerOptions.threshold = thresholdSets[0];
  wrapper.appendChild(element);
  const component = createComponentState(element);
  element.classList.remove("hidden");
}
